import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;


public class Parse {

	public static void main(String[] args) {


		FileWriter fw = null;

		int SSN=0,Name=0,Age=0,Address=0,Surname=0;

		List<String> l = getIzquierda();

		List<String> l2 = getIzquierdaYd();

		Object[] o; 
		String s="";

		System.out.print("\nVector izquierda: \n"+l+" - Tamaño: " + l.size() +"\n");		
		System.out.print("\nVector izquierdaYd: \n"+l2+"Tamaño: "+l2.size()+"\n");
		System.out.println("\n\n");

		//Descomponer metodo:

		//		l2.removeAll();

		while(l.size()>1){
			System.out.println(l);System.out.println(l2);
			o=rVector(l, l2);
			if(o[0].equals("SSN ")) SSN=(int) o[1];
			if(o[0].equals("Name ")) Name=(int) o[1];
			if(o[0].equals("Address ")) Address=(int) o[1];
			if(o[0].equals("Surname ")) Surname=(int) o[1];
			if(o[0].equals("Age ")) Age=(int) o[1];
		}
		System.out.println(l);
		System.out.println(l2);

		//System.out.println(getIzquierda());
		System.out.println("\nSSN: "+SSN +"\nName: "+Name+"\nAdress: "+Address+"\nAge: "+Age+"");
		//System.out.println(getIzquierdaYd());
		//System.out.println(l);

		try{
			fw = new FileWriter("/Users/rodri/Desktop/Fondamenti-di-Data-Science/FINAL/Results/anonymity.txt");
			fw.append("\nSSN: "+SSN +"\nName: "+Name+"\nAdress: "+Address+"\nAge: "+Age);
		} catch (Exception e) {
			System.err.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		}finally {
			try{
				fw.flush();
				fw.close();
			}catch(Exception e){
				System.err.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}

	}
	
	/**Comparamos que el atributo que obtenemos de primera posicion en la lista, si está, lo borramos*/
	public static Object[] rVector(List<String>l,List<String>l2){ //Le pasamos las lista de atributos y el tamaño
		String s = l.get(0); //Sacamos el atributo a comparar (desde arriba y con preferencia a la izquierda)
		int conta=0;	
		Object[] v = {"",""};
		int j=0;
		while(j<l.size()){
			if(s.equals(l.get(j)) || s.equals(l2.get(j))){
				l.remove(j);
				l2.remove(j);
				j=0;
				conta++;
			}else{
				j++;}
		}
		v[0]=s; //Posicion primera = atributo
		v[1]=conta;   //Posicion segunda = repeticion de atributo
		return v;
	}

	public static void cleanLists(Object [] o, List<String>l,List<String>l2){

		for (int i = 0; i < l.size(); i++) {
			System.out.print(o[i]+" ");
			if(o[i].equals("1")){
				l.remove(i);
				l2.remove(i);
			}
		}
	}

	public static List<String> getIzquierda(){
		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/FINAL/izquierda1.txt");
		Scanner s = null;
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				data.add(linea +" ");
				conta++;
			}
			System.out.print(data.get(conta)+ ";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		//System.out.print("  Numero "+i+" de Street  ");
		return  data;
	}

	public static List<String> getIzquierdaYd(){
		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/FINAL/izquierdaYd2.txt");
		Scanner s = null;
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				int n = (int) (Math.random() * 50) + 1;
				data.add(linea +" ");
				conta++;
			}
			//System.out.print(data.get(i)+ ";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		//System.out.print("  Numero "+i+" de Street  ");
		return  data;
	}

}
