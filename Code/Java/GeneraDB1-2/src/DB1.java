import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DB1 {

	private static final String HEADER = "age;workclass;education;marital-status;occupation;relationship;race;sex;capital-gain;native-country;name;surname;street-adress;ssn";

	private static final String HEADER2 = "name;surname;ssn";


	public static void main(String[] args) {

		FileWriter fw = null;
		FileWriter fw2 = null;

		int c=1;
		String nom="";
		String cog="";
		String ssn="";

		Scanner t = new Scanner(System.in);

		System.out.println("Quanti personi bisogni?");

		int length=t.nextInt();

		String x="";
		String y ="";

		try {
			fw = new FileWriter("/Users/rodri/Desktop/DB-Compila/1.txt");
			fw2 = new FileWriter("/Users/rodri/Desktop/DB-Compila/2.txt",true); //Solo nome, cog e ssn
			fw.append(HEADER);
			fw2.append(HEADER2);
			for (int i = 1; i <= length; i++) {
				nom=getNome(NumeroDeNomeRandom());
				cog=getCognome(NumeroDeCognomeRandom());
				ssn=getSSN();
				x="\n"+getPerson(i)+nom+cog+getStreet(NumeroDeStreetRandom())+ssn;
				fw.append(x);
				if(c<=210){
					y="\n"+nom+cog+ssn;
					fw2.append(y);
				}
				System.out.print("\n"+c+x);
				c++;

			}

			//System.out.println(fw.toString());
		} catch (IOException e) {
			System.err.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		}finally {
			try{
				fw2.flush();
				fw.flush();

				fw2.close();
				fw.close();

			}catch(IOException e){
				System.err.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}

		//stampaTuttoConOrdine(length);
		//stampaTuttoSenzaOrdine(length);



	}

	public static String getStreet(int i){
		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Databases Domenico/Street_Names.csv");
		Scanner s = null;
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				int n = (int) (Math.random() * 50) + 1;
				data.add(linea +" "+  n );
				conta++;
			}
			//System.out.print(data.get(i)+ ";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		//System.out.print("  Numero "+i+" de Street  ");
		return data.get(i)+";";
	}

	public static String getCognome(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Databases Domenico/CSV_Database_of_Last_Names.csv");
		Scanner s = null;
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+ ";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		return data.get(i)+";";
	}

	public static String getNome(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Databases Domenico/CSV_Database_of_First_Names.csv");
		Scanner s = null;
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+ ";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		//System.out.print("  Numero "+i+" de Nome  ");
		return data.get(i)+";";

	}

	public static String getSSN(){
		String ssn="";
		for (int i = 0; i < 9; i++) {
			if(i==3 || i==5){ssn+="-";}
			ssn= ssn + RandomN();
		}
		//System.out.print(ssn);
		return ssn;
	}

	public static int RandomN(){ //con cero numero = (int) (Math.random() * n) + 1; = sin ceros
		return (int) (Math.random() * 9);
	}

	public static int NumeroDePersonaRandom(){
		return (int) (Math.random() * 31000) + 1;
	}
	public static int NumeroDeNomeRandom(){
		return (int) (Math.random() * 5494) + 1;
	}
	public static int NumeroDeCognomeRandom(){
		return (int) (Math.random() * 88799) + 1;
	}
	public static int NumeroDeStreetRandom(){
		return (int) (Math.random() * 2659) + 1;
	}


	public static String getPerson(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Databases Domenico/adult_base.csv");
		Scanner s = null;
		String st="";
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		return data.get(i)+";";
	}

	public static String imprimePersona(int i){
		String s = getPerson(i)+getNome(NumeroDeNomeRandom())+getCognome(NumeroDeCognomeRandom())+getStreet(NumeroDeStreetRandom())+getSSN(); //cambiar i en Nome, Cognome y Street
		return s;
	}

	public static String stampaTuttoConOrdine(int length){ //Meter este bucle for dentro del main para optimizar
		String s="";
		for (int i = 1; i <= length; i++) {
			imprimePersona(i);
			s+="\n"+imprimePersona(length);
		}
		return s;
	}

	public static String imprimePersonaRandom(int numeroDePersonas){
		return getPerson(NumeroDePersonaRandom())+getNome(NumeroDeNomeRandom())+getCognome(NumeroDeCognomeRandom())+getStreet(NumeroDeStreetRandom())+getSSN();
	}

	public static String stampaTuttoSenzaOrdine(int length){
		String s="";
		for (int i = 1; i <= length; i++) {
			imprimePersonaRandom(length);
			s+= "\n"+imprimePersonaRandom(length);
		}
		return s;

	}
}