import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/*-Primero coger una posición aleatoria del vector
(Algoritmo que hace random con el tamaño y con esa posición)
- Crear nuevo vector de un tamaño igual que el anterior menos uno y pasar todos lo valores del primero menos el que cojimos antes
-Repetimos operación con el vector más pequeño
-Comprobamos length después de quitar , 
cuando sea 1 insertamos último valor y 
acabamos (booleano que se compruebe cada iteración si está en true, estando primero en false)*/

public class DB4 {

	private static final String HEADER = "BusinessTravel,Department,DistanceFromHome,EnvironmentSatisfaction,HourlyRate,JobLevel,JobRole,JobSatisfaction,MonthlyIncome,NumCompaniesWorked,TotalWorkingYears,YearsAtCompany;name;surname;ssn";


	public static void main(String[] args) {

		FileWriter fw = null;

		int pos=0;
		String p;
		int length=210;
		int [] v =new int [210];
		int [] v2;

		for (int i = 0; i < 210; i++) {
			v [i] = i;
		}

		try{
			fw = new FileWriter("/Users/rodri/Desktop/Fondamenti-di-Data-Science/DB-Compila/DB4.txt");
			fw.write(HEADER);	

			for (int i = 1; i <= 210; i++) { //Bucle que recorra las personas con cancer y que en cada iteracion cree un vector más pequeño

				pos=(int) (Math.random()*length-1)+1; //Hacemos random de todo el vector

				p="\n"+getEmployee(i)+getDBNCS(pos); //Obetenemos la posicin random de la segunda db
				fw.append(p);
				System.out.println(i+"-"+p);

				v2=returnVector(pos, v); //Creamos nuevo vector con una posicion menos
				length--;
			}

			for (int i = 211; i <= 1470; i++) {
				p="\n"+getEmployee(i)+getNome(NumeroDeNomeRandom())+getCognome(NumeroDeCognomeRandom())+getSSN();
				fw.append(p); 
				System.out.println(i+"-"+p);
			}

		} catch (IOException e) {
			System.err.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		}finally {
			try{
				fw.flush();
				fw.close();
			}catch(IOException e){
				System.err.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}

		//stampaTuttoConOrdine(length);
		//stampaTuttoSenzaOrdine(length);

		

	}

	/*Método que te devuelve un vector igual al segundo parámetro menos la posición del primer parametro*/

	public static int [] returnVector(int pos,int [] v){ //PARÁMETROS: tamaño vector, posicion a quitar y vector viejo

		int [] n = new int [v.length]; //Creamos vector tamaño del viejo menos uno

		for (int i : v) { //Recorremos el antiguo pasando los valores
			if(v[i]!=pos) //El valor que sea igual no lo mete, los demás sí
				n[i]=v[i]; //Pasamos cada una de las posiciones 
		}

		return n;
	}

	public static String getEmployee(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/Databases Domenico/Employees.csv");
		Scanner s = null;
		String st="";
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		return data.get(i)+";";
	}



	
	




	public static String getStreet(int i){
		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/Databases Domenico/Street_Names.csv");
		Scanner s = null;
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				int n = (int) (Math.random() * 50) + 1;
				data.add(linea +" "+  n );
				conta++;
			}
			//System.out.print(data.get(i)+ ";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		//System.out.print("  Numero "+i+" de Street  ");
		return data.get(i)+";";
	}

	public static String getCognome(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/Databases Domenico/CSV_Database_of_Last_Names.csv");
		Scanner s = null;
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+ ";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		return data.get(i)+";";
	}

	public static String getNome(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/Databases Domenico/CSV_Database_of_First_Names.csv");
		Scanner s = null;
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+ ";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		//System.out.print("  Numero "+i+" de Nome  ");
		return data.get(i)+";";

	}



	public static String getSSN(){
		String ssn="";
		for (int i = 0; i < 9; i++) {
			if(i==3 || i==5){ssn+="-";}
			ssn= ssn + RandomN();
		}
		//System.out.print(ssn);
		return ssn;
	}

	public static int RandomN(){ //con cero numero = (int) (Math.random() * n) + 1; = sin ceros
		return (int) (Math.random() * 9);
	}

	public static int NumeroDePersonaRandom(){
		return (int) (Math.random() * 31000) + 1;
	}
	public static int NumeroDeNomeRandom(){
		return (int) (Math.random() * 5494) + 1;
	}
	public static int NumeroDeCognomeRandom(){
		return (int) (Math.random() * 88799) + 1;
	}
	public static int NumeroDeStreetRandom(){
		return (int) (Math.random() * 2659) + 1;
	}
	public static int NumeroDeDbncsRandom(){
		return (int) (Math.random() * 209) + 1;
	}


	public static String getSickPerson(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/Databases Domenico/Cancer.csv");
		Scanner s = null;
		String st="";
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		return data.get(i)+";";
	}

	public static String getPerson(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/Databases Domenico/adult_base.csv");
		Scanner s = null;
		String st="";
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		return data.get(i)+";";
	}




	public static String getDBNCS(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/DB-Compila/DB2.txt");
		Scanner s = null;
		String st="";
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		return data.get(i);
	}

	public static String imprimePersona(int i){
		String s = getPerson(i)+getNome(NumeroDeNomeRandom())+getCognome(NumeroDeCognomeRandom())+getStreet(NumeroDeStreetRandom())+getSSN(); //cambiar i en Nome, Cognome y Street
		return s;
	}

	public static String stampaTuttoConOrdine(int length){ //Meter este bucle for dentro del main para optimizar
		String s="";
		for (int i = 1; i <= length; i++) {
			imprimePersona(i);
			s+="\n"+imprimePersona(length);
		}
		return s;
	}

	public static String imprimePersonaRandom(int numeroDePersonas){
		return getPerson(NumeroDePersonaRandom())+getNome(NumeroDeNomeRandom())+getCognome(NumeroDeCognomeRandom())+getStreet(NumeroDeStreetRandom())+getSSN();
	}

	public static String stampaTuttoSenzaOrdine(int length){
		String s="";
		for (int i = 1; i <= length; i++) {
			imprimePersonaRandom(length);
			s+= "\n"+imprimePersonaRandom(length);
		}
		return s;

	}
}