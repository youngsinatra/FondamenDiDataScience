import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Scanner;

/*-Primero coger una posición aleatoria del vector
(Algoritmo que hace random con el tamaño y con esa posición)
- Crear nuevo vector de un tamaño igual que el anterior menos uno y pasar todos lo valores del primero menos el que cojimos antes
-Repetimos operación con el vector más pequeño
-Comprobamos length después de quitar , 
cuando sea 1 insertamos último valor y 
acabamos (booleano que se compruebe cada iteración si está en true, estando primero en false)*/

public class DB5 {

	private static final String HEADER = "name;surname;ssn;product;date;quantity";
	
	public static void main(String[] args) {

		FileWriter fw = null;
		String p;
		int pos,r,k;

		try{
			fw = new FileWriter("/Users/rodri/Desktop/Fondamenti-di-Data-Science/DB-Compila/DB5-.txt");
			fw.append(HEADER);
			k=1;
			while (k<=210) { //210 trabajadores random pero que se repita de 0 - 5
				
				r =(int) (Math.random()*5)+1;
				
				pos=NumeroDeDbncsRandom(); //Random
				
				for (int j = 0; j < r; j++) {
					p="\n"+getDBNCS(pos)+getProduct((int) (Math.random()*88475)+1)+getDate()+";"+(int)((Math.random()*20)+1); //Obtenemos la posicion random de la segunda db
					
					fw.append(p);
					fw.flush();
					System.out.print("\n"+k+"-"+p);
					k++;
				}
								
			}

			for (int l = 211; l <= 88475; l++) {
				p="\n"+getNome(NumeroDeNomeRandom())+getCognome(NumeroDeCognomeRandom())+getSSN()+getProduct(k)+getDate()+";"+(int)((Math.random()*20)+1);
				fw.append(p);
				System.out.print("\n"+l+"-"+p);
			}

		} catch (Exception e) {
			System.err.println("Error in CsvFileWriter !!!");
			e.printStackTrace();
		}finally {
			try{
				fw.flush();
				fw.close();
			}catch(Exception e){
				System.err.println("Error while flushing/closing fileWriter !!!");
				e.printStackTrace();
			}
		}

		//stampaTuttoConOrdine(length);
		//stampaTuttoSenzaOrdine(length);


	}

	/*Método que te devuelve un vector igual al segundo parámetro menos la posición del primer parametro*/

	public static int [] returnVector(int pos,int [] v){ //PARÁMETROS: tamaño vector, posicion a quitar y vector viejo

		int [] n = new int [v.length]; //Creamos vector tamaño del viejo menos uno

		for (int i : v) { //Recorremos el antiguo pasando los valores
			if(v[i]!=pos) //El valor que sea igual no lo mete, los demás sí
				n[i]=v[i]; //Pasamos cada una de las posiciones 
		}

		return n;
	}
	
	public static String getDate(){
		GregorianCalendar gc = new GregorianCalendar();

        int year = 20;

        //gc.set(Calendar.YEAR, year);

        int dayOfYear = randBetween(1, 30);

        gc.set(Calendar.DAY_OF_YEAR, dayOfYear);

        //System.out.println(gc.get(gc.YEAR) + "-" + (gc.get(gc.MONTH) + 1) + "-" + gc.get(gc.DAY_OF_MONTH));
        return + gc.get(Calendar.DAY_OF_MONTH) + "-" + (gc.get(Calendar.MONTH) + 1) + "-2018" ;

    }
	
	public static int randBetween(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }


	



	public static String getProduct(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/Databases Domenico/Products.csv");
		Scanner s = null;
		String st="";
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				String product=""; Boolean b=false;
				int countP=0;
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				for (int j = 0; j < linea.length(); j++) {
					if(linea.charAt(j)==';'){
						countP++;
						if(countP==5){
							b=true;
						}
						if(countP>5){
							b=false;
						}
					}
					if(b==true) product+=linea.charAt(j);
				}
				data.add(product);
				conta++;
			}
			//System.out.print(data.get(i)+";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		return data.get(i)+";";
	}



	public static String getEmployee(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/Databases Domenico/Employees.csv");
		Scanner s = null;
		String st="";
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		return data.get(i)+";";
	}









	public static String getStreet(int i){
		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/Databases Domenico/Street_Names.csv");
		Scanner s = null;
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				int n = (int) (Math.random() * 50) + 1;
				data.add(linea +" "+  n );
				conta++;
			}
			//System.out.print(data.get(i)+ ";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		//System.out.print("  Numero "+i+" de Street  ");
		return data.get(i)+";";
	}

	public static String getCognome(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/Databases Domenico/CSV_Database_of_Last_Names.csv");
		Scanner s = null;
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+ ";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		return data.get(i)+";";
	}

	public static String getNome(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/Databases Domenico/CSV_Database_of_First_Names.csv");
		Scanner s = null;
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+ ";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		//System.out.print("  Numero "+i+" de Nome  ");
		return data.get(i)+";";

	}



	public static String getSSN(){
		String ssn="";
		for (int i = 0; i < 9; i++) {
			if(i==3 || i==5){ssn+="-";}
			ssn= ssn + RandomN();
		}
		//System.out.print(ssn);
		return ssn;
	}

	public static int RandomN(){ //con ceros, numero = (int) (Math.random() * n) + 1; = sin ceros
		return (int) (Math.random() * 9);
	}
	
	public static int RandomS(){ //sin ceros, numero = (int) (Math.random() * n) + 1; = sin ceros
		return (int) (Math.random() * 9)+1;
	}

	public static int NumeroDePersonaRandom(){
		return (int) (Math.random() * 31000) + 1;
	}
	public static int NumeroDeNomeRandom(){
		return (int) (Math.random() * 5494) + 1;
	}
	public static int NumeroDeCognomeRandom(){
		return (int) (Math.random() * 88799) + 1;
	}
	public static int NumeroDeStreetRandom(){
		return (int) (Math.random() * 2659) + 1;
	}
	public static int NumeroDeDbncsRandom(){
		return (int) (Math.random() * 209) + 1;
	}


	public static String getSickPerson(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/Databases Domenico/Cancer.csv");
		Scanner s = null;
		String st="";
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		return data.get(i)+";";
	}

	public static String getPerson(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/Databases Domenico/adult_base.csv");
		Scanner s = null;
		String st="";
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		return data.get(i)+";";
	}




	public static String getDBNCS(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/DB-Compila/DB2.txt");
		Scanner s = null;
		String st="";
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				data.add(linea);
				conta++;
			}
			//System.out.print(data.get(i)+";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		return data.get(i);
	}
	
	public static String getDBNC(int i){

		// Fichero del que queremos leer
		File fichero = new File("/Users/rodri/Desktop/Fondamenti-di-Data-Science/DB-Compila/DB2.txt");
		Scanner s = null;
		String st="";
		List<String> data= new ArrayList<String>();
		int conta=0;

		try {
			// Leemos el contenido del fichero
			s = new Scanner(fichero);

			// Leemos linea a linea el fichero
			while (s.hasNextLine()) {
				String linea = s.nextLine();	// Guardamos la linea en un String
				int count=0;
				String r="";
				//System.out.println(conta +" "+linea);      // Imprimimos la linea
				for (int j = 0; j < linea.length(); j++) {
					if(linea.charAt(j)==';'){
						count++;
						if(count==2){
							r=linea.substring(0, j);
						}
					}
				}
				data.add(r);
				conta++;
			}
			//System.out.print(data.get(i)+";");

		} catch (Exception ex) {
			System.out.println("Mensaje: " + ex.getMessage());
		} finally {
			// Cerramos el fichero tanto si la lectura ha sido correcta o no
			try {
				if (s != null)
					s.close();
			} catch (Exception ex2) {
				System.out.println("Mensaje 2: " + ex2.getMessage());
			}
		}
		return data.get(i);
	}

	public static String imprimePersona(int i){
		String s = getPerson(i)+getNome(NumeroDeNomeRandom())+getCognome(NumeroDeCognomeRandom())+getStreet(NumeroDeStreetRandom())+getSSN(); //cambiar i en Nome, Cognome y Street
		return s;
	}

	public static String stampaTuttoConOrdine(int length){ //Meter este bucle for dentro del main para optimizar
		String s="";
		for (int i = 1; i <= length; i++) {
			imprimePersona(i);
			s+="\n"+imprimePersona(length);
		}
		return s;
	}

	public static String imprimePersonaRandom(int numeroDePersonas){
		return getPerson(NumeroDePersonaRandom())+getNome(NumeroDeNomeRandom())+getCognome(NumeroDeCognomeRandom())+getStreet(NumeroDeStreetRandom())+getSSN();
	}

	public static String stampaTuttoSenzaOrdine(int length){
		String s="";
		for (int i = 1; i <= length; i++) {
			imprimePersonaRandom(length);
			s+= "\n"+imprimePersonaRandom(length);
		}
		return s;

	}
}